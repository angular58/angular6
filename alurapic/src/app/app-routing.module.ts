import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {PhotoListComponent} from './photos/photo-list/photo-list.component';
import {PhotoFormComponent} from './photos/photo-form/photo-form.component';
import {NotFoundComponent} from './errors/not-found/not-found.component';
import {PhotoListResolver} from './photos/photo-list/photo.list.resolver';
import {RequiresAuthenticationGuard} from "./core/auth/requires-authentication.guard";
import {PhotoDetailComponent} from "./photos/photo-detail/photo-detail.component";
import {GlobalErrorComponent} from './errors/global-error/global-error.component';


const routes: Routes = [
  {
    path: '',
    pathMatch: 'full', // significa que a rota deve ser exatamente igual
    redirectTo: 'home'
  },
  {
    path: 'home',
    loadChildren: './home/home.module#HomeModule' // exemplo de lazy loading
  },
  {
    path: 'user/:userName', 
    component: PhotoListComponent, 
    resolve: {
      photos: PhotoListResolver // propriedade: classe resolver
    },
    // na propriedade data pode colocar qualquer coisa, nesse caso, title é o título da página
    data: {
      title: 'Timeline'
    }
  },
  {
    path: 'p/add', 
    component: PhotoFormComponent,
    canActivate: [RequiresAuthenticationGuard],
    data: {
      title: 'Photo Upload'
    }
  },
  {
    path: 'p/:photoId',
    component: PhotoDetailComponent
  },
  {
    path: 'not-found',
    component: NotFoundComponent
  },
  {
    path: 'error',
    component: GlobalErrorComponent,
    data: {
      title: 'Error'
    }
  },
  {
    path: '**', 
    redirectTo: 'not-found'
  }
];

@NgModule({
  // coloca hash na url caso o back-end não esteja preparado para o angular 
  // RouterModule.forRoot(routes, {userHash: true}) 
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
