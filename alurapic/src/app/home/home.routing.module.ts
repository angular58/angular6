import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from "./home.component";
import {SigninCompoent} from "./signin/signin.component";
import {SingupComponent} from "./singup/singup.component";
import {LogginGuard} from '../core/auth/loggin.guard';

// arquivo para realização de lazy loading
const routes: Routes = [
  {
    path: '', // no lazy loading, deve ficar em branco, pois está subordinada a rota pai
    component: HomeComponent,
    canActivate: [LogginGuard],
    children: [
      {
        path: '',
        component: SigninCompoent
      },
      {
        path: 'singup',
        component: SingupComponent
      }
    ]
  }
];

@NgModule({
  // coloca hash na url caso o back-end não esteja preparado para o angular
  // RouterModule.forRoot(routes, {userHash: true})
  // No Lazy loading, usa-se RouterModule.forChild ao invés de RouterModule.forRoot
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
