import { Injectable } from "@angular/core";
import { SingupService } from "./signup.service";
import { AbstractControl } from "@angular/forms";

import { debounceTime, switchMap, map, first } from 'rxjs/operators'

/*
* @Injectable({providedIn: 'root'}) => Indica
* que estará disponível para toda a aplicação (global).
* O provider desse serviço está em HomeModule para não ficar com escopo global,
* ele ficará disponível para todos declarados no módulo.
*/
@Injectable()
export class UserNotTakenValidatorService {

    constructor(
        private singupService: SingupService
    ) {}

    checkUserNameTaken() {
        return (control: AbstractControl) => {
            return control.valueChanges
                .pipe(debounceTime(300))
                .pipe(switchMap(userName => {
                    return this.singupService.checkUserNameTaken(userName);
                }))
                .pipe(map(isTaken => isTaken ? {userNameTaken: true} : null))
                .pipe(first()); // força o complete do observable chamando first
        }
    }

}
