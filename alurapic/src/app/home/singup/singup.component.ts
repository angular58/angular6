import {Component, ElementRef, OnInit, ViewChild} from "@angular/core";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {lowerCaseValidator} from "../../shared/validators/lower-case.validator";
import {UserNotTakenValidatorService} from "./user-not-taken.validator.service";
import {NewUser} from "./new-user";
import {SingupService} from "./signup.service";
import {Router} from "@angular/router";

@Component({
    templateUrl: './singup.component.html',
    providers: [UserNotTakenValidatorService]
})
export class SingupComponent implements OnInit {

    singupForm: FormGroup;
    @ViewChild('emailInput') emailInput: ElementRef<HTMLInputElement>;

    constructor(
        private formBuilder: FormBuilder,
        private userNotTakenValidatorService: UserNotTakenValidatorService,
        private singupService: SingupService,
        private router: Router
    ) {}

    ngOnInit(): void {
        /*
            Parâmetros do array do validador:
            1- valor padrão
            2- validadores síncronos
            3- validadores assíncronos
        */
        this.singupForm = this.formBuilder.group({
            email: ['', 
                [
                    Validators.required,
                    Validators.email
                ]
            ],
            fullName: ['', 
                [
                    Validators.required,
                    Validators.minLength(2),
                    Validators.maxLength(40)
                ]
            ],
            userName: ['', 
                [
                    Validators.required,
                    Validators.minLength(2),
                    Validators.maxLength(30),
                    lowerCaseValidator
                ],
                [
                    this.userNotTakenValidatorService.checkUserNameTaken()
                ]
            ],
            password: ['', 
                [
                    Validators.required,
                    Validators.minLength(8),
                    Validators.maxLength(14)
                ]
            ]
        });
        this.emailInput.nativeElement.focus();

    }

    /* VALIDACAO EXIBIDA NA INSTANTANEAMENTE NO FORMULÁRIO
    singup() {
        // getRowValue retorna objeto json do formulário
        const newUser = this.singupForm.getRawValue() as NewUser;
        this.singupService.singup(newUser)
            .subscribe(
                () => this.router.navigate(['']),
                err => console.log(err)
            );
    }
    */

    // VALIDACAO EXIBIDA APÓS O SUBMIT
    singup() {

      if (this.singupForm.valid && !this.singupForm.pending) {
        // getRowValue retorna objeto json do formulário
        const newUser = this.singupForm.getRawValue() as NewUser;
        this.singupService.singup(newUser)
          .subscribe(
            () => this.router.navigate(['']),
            err => console.log(err)
          );
      } else {

      }


    }
}
