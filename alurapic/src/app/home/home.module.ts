import {NgModule} from "@angular/core";
import {SigninCompoent} from "./signin/signin.component";
import {FormsModule, ReactiveFormsModule} from '@angular/forms'
import {CommonModule} from "@angular/common";
import {VMessageModule} from "../shared/components/vmessage/vmessage.module";
import {RouterModule} from "@angular/router";
import {SingupComponent} from "./singup/singup.component";
import {HomeComponent} from "./home.component";
import {HomeRoutingModule} from "./home.routing.module";
import {SingupService} from "./singup/signup.service";

@NgModule({
    declarations: [
        SigninCompoent,
        SingupComponent,
        HomeComponent
    ],
    imports: [
        CommonModule,
        ReactiveFormsModule,
        VMessageModule,
        RouterModule,
        FormsModule,
        HomeRoutingModule
    ],
  providers: [
    SingupService
  ]
})
export class HomeModule {

}
