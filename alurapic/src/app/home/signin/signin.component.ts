import { Component, OnInit, ElementRef, ViewChild } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import { AuthService } from "../../core/auth/auth.service";
import { PlataformDetectorService } from "../../core/plataform-detector/plataform-detector.service";
import {Title} from "@angular/platform-browser";

@Component({
    templateUrl: './signin.component.html'
})
export class SigninCompoent implements OnInit {

    loginForm: FormGroup;
    @ViewChild('userNameInput') userNameInput: ElementRef<HTMLInputElement>;
    fromUrl: string;
    
    constructor(
        private formBuilder: FormBuilder,
        private authService: AuthService,
        private router: Router,
        private plataformDetectorService: PlataformDetectorService,
        private titleService: Title, // usado para alterar o title da página
        private activatedRoute: ActivatedRoute
    ) { }
    
    ngOnInit(): void {
        this.activatedRoute.queryParams.subscribe(params => {
        this.fromUrl = params['fromUrl'];
      });
      this.titleService.setTitle('Login');
        this.loginForm = this.formBuilder.group({

            userName: ['', Validators.required],
            password: ['', Validators.required]
        });
        this.userNameInput.nativeElement.focus();
    }

    login() :void {
        const userName = this.loginForm.get('userName').value;
        const password = this.loginForm.get('password').value;
        this.authService.authenticate(userName, password)
            .subscribe(
                () => {
                    // this.router.navigateByUrl('user/' + userName)

                  if (this.fromUrl) {
                    this.router.navigateByUrl(this.fromUrl);
                  } else {
                    this.router.navigate(['user', userName]); // 'user/' + userName
                  }

                },
                err => { 
                    console.log(err); 
                    this.loginForm.reset(); 
                    /*
                    if (this.plataformDetectorService.isPlataformBrowser()) {
                        this.userNameInput.nativeElement.focus();
                    }
                    */
                   this.userNameInput.nativeElement.focus();
                    alert('invalid username or password'); 
                }); 
    }

}
