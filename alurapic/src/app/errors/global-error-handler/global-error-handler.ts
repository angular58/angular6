import {ErrorHandler, Injectable, Injector} from "@angular/core";
import * as Stacktrace from 'stacktrace-js';
import {LocationStrategy, PathLocationStrategy} from "@angular/common";
import {UserService} from "../../core/user/user.service";
import {Router} from '@angular/router';
import {environment} from '../../../environments/environment';

/* está registrado no error.module */
@Injectable()
export class GlobalErrorHandler implements  ErrorHandler {

  /* injeção de dependência sob demanda, similar ao Instance<T> do CDI */
  constructor(private injector: Injector) { }

  handleError(error: any): void {
    const location = this.injector.get(LocationStrategy);
    const userService = this.injector.get(UserService);
    const url = location instanceof PathLocationStrategy ? location.path() : '' ;
    const message = error.message ? error.message : error.toString();
    const router = this.injector.get(Router);

    if (environment.production) {
      router.navigate(['/error']);
    }

    Stacktrace
      .fromError(error)
      .then(stackFrames => {
        const stackAsString = stackFrames
          .map(st => st.toString())
          .join('\n');
        console.log(message);
        console.log(stackAsString);

        /* seria enviado para o backend para log */
        console.log({message, url, userName: userService.getUserName(), stack: stackAsString});
      });
  }

}
