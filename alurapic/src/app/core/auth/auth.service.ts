import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { tap } from 'rxjs/operators'
import { TokenService } from '../token/token.service';
import { UserService } from '../user/user.service';

const API_URL = 'http://localhost:3000';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private http: HttpClient,
    private userService: UserService
  ) { }

  authenticate(userName: string, password: string)   {
    /*
      Pipe: entre a execução da aplicação e o subscribe, pode-se executar 
      um código arbitrário, como filtro etc.
      o post tem três parâmetros
      1- URL do serviço
      2- objeto a ser enviado
      3- (opcional) configuração do request
    */
    return this.http
      .post(API_URL + '/user/login', 
        {userName: userName, password: password}, 
        {observe: 'response'})
      .pipe(tap(res => {
        const authToken = res.headers.get('x-access-token');
        this.userService.setToken(authToken);
        console.log('authToken = ' + authToken);
      }))
  }

}
