import { Injectable, PLATFORM_ID, Inject } from "@angular/core";
import { isPlatformBrowser } from "@angular/common"

@Injectable({
    providedIn: 'root'
})
export class PlataformDetectorService {

    // injeção de dependência por identificador
    constructor(@Inject(PLATFORM_ID) private plataformid: string) { } 

    isPlataformBrowser() {
        return isPlatformBrowser(this.isPlataformBrowser);
    }

}