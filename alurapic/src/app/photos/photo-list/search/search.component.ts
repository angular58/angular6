import { Component, OnInit, OnDestroy, Output, EventEmitter, Input } from "@angular/core";
import { Subject } from "rxjs";
import { debounceTime } from 'rxjs/operators';

@Component({
    selector: 'ap-search',
    templateUrl: './search.component.html'
})
export class SearchComponent implements OnInit, OnDestroy {


    @Output() onDigitar = new EventEmitter<string>();
    @Input() value: string = '';
    debounce: Subject<string> = new Subject<string>();
    
    ngOnInit(): void {
        // debounce é o delay da digitação usado no filtro da view
        this.debounce
        .pipe(debounceTime(300))
        .subscribe(filtro => this.onDigitar.emit(filtro)); 
    }
    
    ngOnDestroy(): void {
        this.debounce.unsubscribe();
    }

}