import { PhotoService } from "../photo/photo.service";
import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { Observable } from "rxjs";
import { Photo } from "../photo/photo";

@Injectable({providedIn: 'root'})
export class PhotoListResolver implements Resolve<Observable<Photo[]>> {
    
    constructor(private service: PhotoService) {
        
    }

    // usado na rota app-routing.module.ts
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        /* Foi mudado para paginação...
        const userName = route.params.userName;
        return this.service.listFromUser(userName);
        */
        const userName = route.params.userName;
        return this.service.listFromUserPaginated(userName, 1);
    }

}