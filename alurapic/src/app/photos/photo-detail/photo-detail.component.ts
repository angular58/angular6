import {Component, OnInit} from "@angular/core";
import {ActivatedRoute, Router} from "@angular/router";
import {PhotoService} from "../photo/photo.service";
import {Photo} from "../photo/photo";
import {Observable} from "rxjs";
import {PhotoComment} from "../photo/photo-comment";
import {AlertService} from "../../shared/components/alert/alert.service";
import {UserService} from "../../core/user/user.service";

@Component({
  selector: 'ap-photo-detail',
  templateUrl: './photo-detail.component.html',
  styleUrls: ['./photo-detail.component.css']
})
export class PhotoDetailComponent implements OnInit {

  photo$: Observable<Photo>;
  photoId: number;

  constructor(
    private activateRoute: ActivatedRoute,
    private photoService: PhotoService,
    private router: Router,
    private alertService: AlertService,
    private userService: UserService
  ) { }

  ngOnInit(): void {
    this.photoId = this.activateRoute.snapshot.params.photoId;
    this.photo$ = this.photoService.findById(this.photoId);

    // apenas para redirecionar caso não exista o id passado na rota
    this.photo$.subscribe(() => {}, err => {
      // replaceUrl = true remove o back history do navegador
      this.router.navigate(['not-found'], {replaceUrl: true});
    })
  }

  remove() {
    this.photoService.removePhoto(this.photoId)
      .subscribe(
        () => {
          this.alertService.success('Photo removed!', true);
          return this.router.navigate(['/user', this.userService.getUserName()]);
      },
        err => this.alertService.danger('Could not delete the photo!', true));
  }

  like(photo: Photo) {
    this.photoService.like(photo.id)
      .subscribe(liked => {
        if (liked) {
          this.photo$ = this.photoService.findById(photo.id);
        }
      });
  }

}
