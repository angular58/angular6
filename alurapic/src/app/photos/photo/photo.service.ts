import { HttpClient, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Photo } from "./photo";
import {PhotoComment} from "./photo-comment";
import {catchError, map} from "rxjs/operators";
import {of, throwError} from "rxjs";
import {environment} from "../../../environments/environment";

const API = environment.apiUrl;

// root indica escopo raiz, ou seja, qualquer componente terá acesso a ele (mesma instância)
@Injectable({providedIn: 'root'}) 
export class PhotoService {

    constructor(private http:HttpClient) {
        this.http = http;
    }

    listFromUser(userName:string) {
        // observable é do tipo lazy
        // const observable = http.get('http://localhost:3000/flavio/photos');

        // quando o subscrible é chamado que é feito a busca dos dados
        // observable.subscribe(); 

        return this.http.get<Photo[]>(API + '/' + userName + '/photos');
    }

    listFromUserPaginated(userName:string, page: number) {
        // passa um parâmetro '?page=2'
        const paramPage = new HttpParams().append('page', page.toString());
        return this.http.get<Photo[]>(API + '/' + userName + '/photos', { params: paramPage });
    }

    upload(description: string, allowComments: boolean, file: File) {
      const formData = new FormData(); // não é objeto do Angular, é do html.
      formData.append('description', description);
      formData.append('allowComments', allowComments.toString());
      formData.append('imageFile', file);

      //return this.http.post(API + '/photos/upload', formData);
      // trocado o return para mostrar barra de progresso
      return this.http.post(API + '/photos/upload',
        formData,
        {
          observe: 'events',
          reportProgress: true
        }
      );

    }

    findById(photoId: number) {
      return this.http.get<Photo>(API + '/photos/' + photoId);
    }

    getComments(photoId: number) {
      return this.http.get<PhotoComment[]>(API + '/photos/' + photoId + '/comments');
    }

    addComment(photoId: number, commentText: string) {
      return this.http.post(API + '/photos/' + photoId + '/comments', {commentText: commentText});
    }

    removePhoto(photoId: number) {
      return this.http.delete(API + '/photos/' + photoId);
    }

    like(photoId: number) {
      /* 3o parâmetro para ler o status da resposta */

      return this.http.post(API + '/photos/' + photoId + '/like', {}, {observe: 'response'})
        .pipe(map(res => true))
        .pipe(catchError(err => {
          return err.status == '304' ? of(false) : throwError(err);
        }))

    }

}
