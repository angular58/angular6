import {Component, OnInit} from "@angular/core";
import {LoadingService} from "./loading.service";
import {Observable} from "rxjs";
import {LoadingType} from "./loading-type";
import {map} from "rxjs/operators";

@Component({
  selector: 'ap-loading',
  templateUrl: './loading.component.html',
  styleUrls: ['./loading.component.css']
})
export class LoadingComponent implements OnInit {

  loading$ = new Observable<string>();

  constructor(
    private loadingService: LoadingService
  ) { }

  ngOnInit(): void {
    // loadingType.valueOf() retorna o valor String da enum
    this.loading$ = this.loadingService.getLoading()
      .pipe(map(loadingType => loadingType.valueOf()));
  }

}
