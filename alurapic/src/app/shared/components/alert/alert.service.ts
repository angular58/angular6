import {Injectable} from "@angular/core";
import {Observable, Subject} from "rxjs";
import {Alert, AlertType} from "./alert";
import {NavigationStart, Router} from "@angular/router";

@Injectable({
  providedIn: 'root'
})
export class AlertService {

  alertSubject: Subject<Alert> = new Subject<Alert>();
  keepAlertRouteChange = false;

  constructor(
    router: Router
  ) {
    router.events.subscribe(event => {
      if (event instanceof NavigationStart) {
        if (this.keepAlertRouteChange) {
          this.keepAlertRouteChange = false;
        } else {
          this.clear();
        }
      }
    })
  }

  success(message: string, keepAlertRouteChange: boolean = false) {
    this.alert(AlertType.SUCCESS, message, keepAlertRouteChange);
  }

  warning(message: string, keepAlertRouteChange: boolean = false) {
    this.alert(AlertType.WARNING, message, keepAlertRouteChange);
  }

  danger(message: string, keepAlertRouteChange: boolean = false) {
    this.alert(AlertType.DANGER, message, keepAlertRouteChange);
  }

  info(message: string, keepAlertRouteChange: boolean = false) {
    this.alert(AlertType.INFO, message, keepAlertRouteChange);
  }

  // emite um objeto alert para os subscritos
  private alert(alertType: AlertType, message: string, keepAlertRouteChange: boolean) {
    this.keepAlertRouteChange = keepAlertRouteChange;
    this.alertSubject.next(new Alert(alertType, message));
  }

  // quem quiser ficar ouvindo o alert, chama esse método
  getAlert(): Observable<Alert> {
    return this.alertSubject.asObservable();
  }

  clear() {
    this.alertSubject.next(null);
  }

}
