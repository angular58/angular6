import { Directive, ElementRef, HostListener, Renderer, Input } from "@angular/core";

@Directive({
    selector: '[apDarkenOnHover]'
})
export class DarkenOnHoverDirective {

    /* Modo de usar
    <h1 style="background: red;" apDarkenOnHover brightness="20%">Photos</h1>
    */
    @Input() brightness = '70%';

    // ElementRef é a referência do elemento na qual a diretiva está adicionada
    constructor(
        private el: ElementRef,
        private render: Renderer
    ) { }

    @HostListener('mouseover')
    darkenOn() {
        this.render.setElementStyle(
            this.el.nativeElement, 'filter', `brightness(${this.brightness})`);
    }

    @HostListener('mouseleave')
    darkenOff() {
        this.render.setElementStyle(this.el.nativeElement, 'filter', 'brightness(100%)');
    }

}