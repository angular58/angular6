import {Directive, ElementRef, OnInit, Renderer} from "@angular/core";
import {UserService} from "../../../core/user/user.service";

@Directive({
  selector: '[showIfLogged]'
})
export class ShowIfLoggedDirective implements OnInit {

  currentDisplay: string;

  constructor(
    private element: ElementRef<any>,
    private renderer: Renderer,
    private userService: UserService
  ) { }

  ngOnInit(): void {
    /* necessário para atualizar componentes que são carregados uma única vez
     * por exemplo, o menu. Com subscrible do userService, pode-se verificar
     * quando existe usuário logado.
     */
    this.currentDisplay = getComputedStyle(this.element.nativeElement).display;

    this.userService.getUser()
      .subscribe(user => {
        if (user) {
          this.renderer.setElementStyle(this.element.nativeElement, 'display', this.currentDisplay)
        } else {
          this.currentDisplay = getComputedStyle(this.element.nativeElement).display;
          this.renderer.setElementStyle(this.element.nativeElement, 'display', 'none');
        }
      });

  }

}
